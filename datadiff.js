function dateDiff(firstDate, secondDate) {

    // преобразуем даты к формату "YYYY-MM-DD"
    let startDate = new Date(new Date(firstDate).toISOString().slice(0, 10));
    // если вторая дата не задана - берем текущую дату
    if (!secondDate) {
        secondDate = new Date().toISOString().slice(0, 10);
    }
    let endDate = new Date(secondDate);
    // проверяем порядок большей-меньшей даты
    if (startDate > endDate) {
        let swap = startDate;
        startDate = endDate;
        endDate = swap;
    }
    // модуль вычисления разницы между датами

    const startYear = startDate.getFullYear();
    // проверка на "высокосность"
    const february = (startYear % 4 === 0 && startYear % 100 !== 0) || startYear % 400 === 0 ? 29 : 28;
    console.log(february);
    const daysInMonth = [31, february, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    let yearDiff = endDate.getFullYear() - startYear;

    let monthDiff = endDate.getMonth() - startDate.getMonth();

    if (monthDiff < 0) {
        yearDiff--;
        monthDiff += 12;
    }

    let dayDiff = endDate.getDate() - startDate.getDate();

    if (dayDiff < 0) {
        if (monthDiff > 0) {
            monthDiff--;
        } else {
            yearDiff--;
            monthDiff = 11;
        }
        dayDiff += daysInMonth[startDate.getMonth()];
    }

    return yearDiff + 'Year(s) ' + monthDiff + 'Month(s) ' + dayDiff + 'Day(s)';
}

// Функция dateDiff - входные параметры - 2 даты (если передана одна дата то вычисляется разница
// с текущей датой. Возвращаемые параметры - разница в годах + разница в месяцах + разница в днях

console.log(dateDiff('1900-02-09'))