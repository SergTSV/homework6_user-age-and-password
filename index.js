const app = document.getElementById('app')
const inputFirstName = app.querySelector('#firstName');
const inputLastName = app.querySelector('#lastName');
const inputBirthday = app.querySelector('#dateOfBirth');
const btnCreateUser = app.querySelector('#createUser');


function createNewUser () {
    const newUser = {
        firstName: '',
        lastName: '',
        birthday: '',

        getLogin() {
           console.log(`${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`);
           return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
        },
        getAge() {
            console.log(`${new Date().getFullYear()}`-`${this.birthday.getFullYear()}`);
            return `${new Date().getFullYear()}`-`${this.birthday.getFullYear()}`;
        },
        getPassword(){
            console.log(`${this.firstName[0]}${this.lastName.toLowerCase()}${this.birthday.getFullYear()}`);
            return `${this.firstName[0]}${this.lastName.toLowerCase()}${this.birthday.getFullYear()}`;
        },
        setFirstName(newFirstName) {
            Object.defineProperty(this, 'firstName', {writable: true});
            this.firstName = newFirstName;
            Object.defineProperty(this, 'firstName', {writable: false});
        },
        setLastName(newLastName) {
            Object.defineProperty(this, 'lastName', {writable: true});
            this.lastName = newLastName;
            Object.defineProperty(this, 'lastName', {writable: false});
        },
    };
    Object.defineProperties(this, {
            firstName: {writable: false},
            lastName: {writable: false},
        }
    );

        btnCreateUser.addEventListener('click', () => {
            if (inputFirstName.value && inputLastName.value) {
                newUser.setFirstName(inputFirstName.value);
                newUser.setLastName(inputLastName.value);
                newUser.birthday = new Date(inputBirthday.value);
                newUser.getLogin();
                newUser.getAge();
                newUser.getPassword();
            } else {
                alert('Input Error. Enter the data again');
            }
        });

    return newUser;
}

getNewUser = createNewUser();










